#!/bin/bash

SCRIPT_PATH=$(dirname $(realpath "$0"))

export CI_MODE=$CI_MODE
trap "unset CI_MODE" EXIT

# CLI_DEV -> Specify if you want to run cli JS code from your local code instead of the one pre-bundled on the docker image
if [ "$CLI_DEV" = "1" ];then 
    # Run cli from local folder & install dependencies
    echo -e "\e[1;31m CLI DEV MODE \e[0m"
    cd /project/cli/node
    yarn
else
    # Normal / Fast mode -> Run cli from docker image directly with pre-installed dependencies
    cd /cli/node
fi

export NODE_NO_WARNINGS=1
npx babel-node --experimental-modules --es-module-specifier-resolution=node -- src/index.js $@