import colorLog from './utils/colorLog'
import bash from './utils/bash'
import { start, stop, logs, restart } from './scripts/stacks'

const CLI_PATH = process.env.CLI_DEV ? '/project/cli' : '/cli'

const cli = async (args) => {
    switch (args[0]) {
        case 'start':
            await start(args.slice(1))
            break;
        case 'stop':
            await stop(args.slice(1))
            break;
        case 'logs':
            await logs(args.slice(1))
            break;
        case 'restart':
            await restart(args.slice(1))
            break;
        default:
            await bash(`${CLI_PATH}/bash/protofy.sh ${args.join(' ')}`, { critical: false })
            //colorLog(`Specified operation ${[...process.argv].splice(2).join(' ')} unrecognized.`, 'error')
            break;
    }
    process.exit(0);
}

const main = async () => {
    const args = [...process.argv].splice(2)
    await cli(args)
}

main().catch(e => {
    colorLog(`${e} `, 'error')
})

const signals = ['SIGINT', 'SIGTERM', 'SIGQUIT']
signals.forEach((signal) => process.on(signal, async () => {
    colorLog("Exiting...")
    process.exit(0);
}));