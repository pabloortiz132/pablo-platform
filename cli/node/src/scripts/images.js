import colorLog from '../utils/colorLog'
import bash from '../utils/bash'

export const pullRequired = async (requiredImages) => {
    for (const tag of Object.keys(requiredImages)) {
        try {
            await bash(`docker pull ${tag} | grep 'Image is up to date'`,
                {
                    silent: true,
                    log: true,
                    alias: `Pulling ${tag} ...`
                }
            )
        } catch {
            colorLog(`Image ${tag} has been updated.`)
        }
    }
}