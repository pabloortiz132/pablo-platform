import * as fs from 'fs'
import mergeYaml from 'merge-yaml'
import bash from '../utils/bash'
import { colorLog } from '../utils/colorLog'
import { pullRequired } from './images'

export const start = async (args, detached = process.env.CI_MODE == '1') => {
    // Run mode
    const prod = !!args.find(a => a == '--prod')
    // Build images locally before starting
    const build = !!args.find(a => a == '--build')
    // Stack to be started
    const stack = args[0]

    const ymlFiles = getStackFiles(stack, prod).map(file => `-f ${file}`).join(' ')
    try {
        await pullImages([stack], prod, build ? 'build' : 'pull')

        await bash(
            `docker-compose ${ymlFiles} -p ${stack}${prod ? '-prod' : '-dev'} up ${detached ? '-d' : ''}`,
            detached
                ? {
                    silent: true,
                    log: true,
                    alias: `Starting stack: ${stack}`
                }
                : {}
        )

    } catch (e) {
        console.error(e)
        colorLog('Exiting...')
    }
}

export const stop = async (args, detached = process.env.CI_MODE == '1') => {
    // Run mode
    const prod = !!args.find(a => a == '--prod')
    // Stack to be stopped
    const stack = args[0]

    const ymlFiles = getStackFiles(stack, prod).map(file => `-f ${file}`).join(' ')
    try {

        await bash(
            `docker-compose ${ymlFiles} -p ${stack}${prod ? '-prod' : '-dev'} down`,
            detached
                ? {
                    silent: true,
                    log: true,
                    alias: `Stopping stack: ${stack}`
                }
                : {}
        )

    } catch (e) {
        console.error(e)
        colorLog('Exiting...')
    }
}

export const logs = async (args) => {
    // Run mode
    const prod = !!args.find(a => a == '--prod')
    // Stack to be logged
    const stack = args[0]

    try {

        await bash(`docker-compose -p ${stack}${prod ? '-prod' : '-dev'} logs -f`)

    } catch (e) {
        console.error(e)
        colorLog('Exiting...')
    }
}

export const restart = async (args) => {
    await stop(args)
    await start(args)
}

export const pullImages = async (stack, prod = false) => {

    const requiredImages = {}
    const ymlFiles = getStackFiles(stack, prod)
    const yamlMerge = mergeYaml(ymlFiles)
    for (const service of Object.values(yamlMerge.services) || []) {
        if (service.build) {
            const tag = service.image
            let path
            if (service.build.context) {
                path = `${service.build.context.replace('${PROJECT_DIR}', '/project')}/${service.build.dockerfile}`
            } else {
                path = `${service.build.replace('${PROJECT_DIR}', '/project')}/Dockerfile`
            }
            requiredImages[tag] = path
        }
    }
    await pullRequired(requiredImages)
}

const getStackFiles = (stack, prod) => {
    return fs.readdirSync(`/project/stacks/${stack}`)
        .filter(f => f.endsWith('.yml') && (!prod || !f.endsWith('.dev.yml')))
        .sort((a, b) => {
            const aDev = a.endsWith('.dev.yml')
            const bDev = b.endsWith('.dev.yml')
            return aDev < bDev ? -1 : 1
        })
        .map(f => `/project/stacks/${stack}/${f}`)
}