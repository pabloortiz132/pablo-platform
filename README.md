# Docker Compose CLI for Easy Project Stacking

This repository contains a Docker-based CLI tool designed to simplify the process of launching Docker Compose projects using docker-compose. The tool enables users to effortlessly manage and start different stacks of services within their projects. The following guide provides an overview of how to use this CLI effectively.

## Installation

Your project should have a 'system.sh' file, which will invoke this CLI. An example of such a file is:
```sh
#!/bin/bash

docker run \
	--rm \
    --name myproject-$$ \
	-ti \
	$entrypointArg \
	--user $(id -u):$(stat -c %g /var/run/docker.sock) \
	-v $PWD:/project \
	$cliMount \
	-v /var/run/docker.sock:/var/run/docker.sock \
	-v ~/.ssh:/home/node/.ssh \
	-e PROJECT_DIR=$PWD \
	-e CI_MODE \
	-e CLI_DEV \
	--network host \
	registry.gitlab.com/pabloortiz132/pablo-platform/platform \
	$@
```

> Additionally, you can pass the CI_MODE=1 environment variable if you're using a pseudo-tty. Your script could also automatically update the docker image before running if you desire to do so.

## Usage

1. **Project Structure**: The repository should have a 'stacks' folder. Each subfolder within 'stacks' represents a stack containing services that can be launched using the CLI.

2. **Stacks and Services**: Inside the 'stacks' folder, each subfolder represents a stack. Within each stack's subfolder, you can organize services by creating individual folders for each service. These service folders contain '.yml' files that define the service configurations.

3. **Starting Stacks**: To start a stack, use the following command in the repository's root directory:
   ```
   ./system start (stack)
   ```
   Replace `(stack)` with the name of the desired stack.

4. **Skipping Development Files**: By default, all '.yml' files within the service folders are added to the Docker Compose project. However, if you wish to exclude development-specific configurations, you can use the `--prod` flag. For example:
   ```
   ./system start (stack) --prod
   ```

5. **Referencing Project Root**: Inside the '.yml' configuration files, you can reference the root of the project using `${PROJECT_DIR}`. This allows you to create relative paths that work across different environments.

## Examples

Suppose you have the following project structure:

```
your-repo/
|-- stacks/
|   |-- stack1/
|   |   |-- service1/
|   |   |   |-- service1.yml
|   |   |   |-- service1.dev.yml
|   |   |-- service2/
|   |       |-- service2.yml
|   |-- stack2/
|       |-- ...
|-- system
|-- ...
```

To start 'stack1' in development mode:
```
./system start stack1
```

To start 'stack2' in production mode (skipping development files):
```
./system start stack2 --prod
```

## Usage examples
I have made this project in order to use it in other projects of mine. One such project is:
* [HomePanel](https://github.com/Pablo-Ortiz-Lopez/HomePanel/tree/master)
## Feedback and Contributions

If you encounter any issues, have suggestions for improvements, or would like to contribute to this project, feel free to open an issue or submit a pull request on the [GitHub repository](https://github.com/Pablo-Ortiz-Lopez/PabloPlatform).

Happy stacking!
